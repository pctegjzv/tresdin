# Tresdin
![logo](./docs/assests/tresdin.png)    
I am alauda job commander, i can schedule all kinds of jobs!

## Features
- instant jobs.

## Requirements
- Container scheduler backend.(chronos or kubernetes)
- virtualenv

## Design
### command 和 entrypoint 设计
要么完全使用镜像的，要么就完全自己写，去掉镜像和自定义命令组合的形式:    

1. 如果不填command，那么就使用镜像的entrypoint和cmd
2. 如果填了command，那么镜像的entrypoint和cmd就都没用了，最终会以shell模式启动任务(/bin/sh -c "<command>")

## db migrate notes
Tresdin is based on **Python3**, and we use virtualenv to manage python envs
1. create python3 virtualenv
```
mkdir -p ~/.virtualenv/tresdin
virtualenv --python path/to/python3 ~/.virtualenv/tresdin
source ~/.virtualenv/tresdin/bin/activate
```
2. install packages
```
pip install -r requirements/base.txt
pip install --trusted-host pypi.alauda.io \
                   --extra-index-url http://mathildetech:Mathilde1861@pypi.alauda.io/simple/ \
                   -r requirements/alauda.txt
```
3. makemigrations
```
cd service
python manage.py makemigrations --settings django_config.settings.dev
```

## Contributions
