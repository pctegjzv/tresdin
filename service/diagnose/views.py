import logging
import os
import requests
import time

from django.conf import settings
from rest_framework import viewsets
from rest_framework.response import Response

from alauda.tracing import tracing_response_time

from jobs.log_client.log_client import ElasticSearchClient
from job_configs.models import JobConfig

LOG = logging.getLogger(__name__)
status_ok = 'OK'
status_error = 'ERROR'


class DiagnoseViewset(viewsets.ViewSet):

    @tracing_response_time()
    def retrieve(self, request):
        self.init_result()
        self.check_env_var()
        self.check_db()
        self.check_jakiro()
        self.check_medusa()
        self.check_es()

        for detail in self.diagonse_result['details']:
            if detail.get('status') == status_error:
                self.diagonse_result['status'] = status_error
                break
        return Response(self.diagonse_result)

    def init_result(self):
        self.diagonse_result = {}
        self.diagonse_result['status'] = status_ok
        self.diagonse_result['details'] = []

    def check_env_var(self):
        start = self._current()
        env_var_list = ['SECRET_KEY', 'FURION_ENDPOINT',
                        'JAKIRO_API_ENDPOINT', 'SVEN_ENDPOINT']
        status = status_ok
        message = ''
        for env_var in env_var_list:
            if not os.getenv(env_var):
                status = status_error
                message += '{} not exist,'.format(env_var)
        attr_list = [
            'JAKIRO_API_VERSION', 'MEDUSA_API_VERSION', 'SVEN_API_VERSION']
        for attr in attr_list:
            if not getattr(settings, attr, None):
                status = status_error
                message += '{} not exist,'.format(attr)
        latency = self._current() - start
        self.add_diagnose_detail(status, 'env_var', message=message, latency=latency)

    def check_db(self):
        start = self._current()
        status = status_ok
        message = ''
        try:
            JobConfig.objects.all().count()
        except:
            status = status_error
            message = 'query in db failed'
        latency = self._current() - start
        self.add_diagnose_detail(status, 'database', message=message, latency=latency)

    def check_jakiro(self):
        endpoint = settings.JAKIRO_API_ENDPOINT
        path = '{}/_ping'.format(endpoint)
        self._check_our_service(path, 'jakiro')

    def check_medusa(self):
        endpoint = settings.MEDUSA_ENDPOINT
        path = '{}/ping'.format(endpoint)
        self._check_our_service(path, 'medusa')

    def check_es(self):
        check_start = self._current()
        status = status_ok
        message = ''
        try:
            es = ElasticSearchClient()
            if not es.conn:
                raise Exception('connect es failed')
            start = time.time()
            end = start + 60
            es.get_job_log('test_job_id', start, end)
        except:
            status = status_error
            message = 'connect es failed'
        latency = self._current() - check_start
        self.add_diagnose_detail(status, 'es', message=message, latency=latency)

    def add_diagnose_detail(self, status, name, message='', suggestion='', latency=''):
        detail = {
            'status': status,
            'name': name,
            'message': message,
            'suggestion': suggestion,
            'latency': '{}ms'.format(latency)
        }
        self.diagonse_result['details'].append(detail)

    def _check_our_service(self, path, name):
        status = status_ok
        message = ''
        latency = 0
        try:
            start = self._current()
            result = requests.get(path, timeout=1)
            if result.status_code >= 300:
                status = status_error
                message = 'call {} failed'.format(name)
            latency = self._current() - start
        except:
            message = 'call {} failed'.format(name)
            status = status_error
        self.add_diagnose_detail(status, name, message=message, latency=latency)

    def _current(self):
        return int(time.time() * 1000)
