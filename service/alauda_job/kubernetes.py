import logging
import time

from django.conf import settings
from kubernetes.client import ApiClient, BatchV1Api, Configuration, CoreV1Api, V1DeleteOptions
from kubernetes.client import V1EnvFromSource
from kubernetes.client import V1EnvVarSource
from kubernetes.client.models import (V1Job, V1JobSpec, V1PodSpec,
                                      V1Container, V1EnvVar, V1PodTemplateSpec,
                                      V1ResourceRequirements, V1Volume, V1VolumeMount,
                                      V1ObjectMeta, V1HostPathVolumeSource,
                                      V1SecurityContext)
from kubernetes.client.rest import ApiException
from event_task.models import EventTask
from jobs.log_client.log_client import get_es_client


logger = logging.getLogger(__name__)


class KubernetesClient:
    def __init__(self, endpoint, token="", is_new_k8s=False):
        self.endpoint = endpoint
        self.token = token
        self.is_new_k8s = is_new_k8s

        self.config = self._get_k8s_config_()
        self.client = BatchV1Api(api_client=ApiClient(configuration=self.config))
        self.pod_client = CoreV1Api(api_client=ApiClient(configuration=self.config))

    def create_job(self, data, **kwargs):
        region_id, log_client = None, None
        job_name = data.get('name')
        namespace = data.get('namespace', 'default')
        
        if 'private-build-' in job_name:
            region_id = data.get('region_id')
            log_client = get_es_client(region_id)
            msg = '[Init][Kubernetes] > start create build job {}'.format(job_name)
            log_client.save_job_log(job_name, msg)

        try:
            self.client.create_namespaced_job(namespace=namespace,
                                              body=self._convert_to_k8s_job_(data),
                                              **self._default_kwargs_)
        except Exception as e:
            raise Exception('create job failed: {}'.format(e))
        
        if 'private-build-' in job_name and log_client:
            msg = '[Init][Kubernetes] > create job {} to k8s success'.format(job_name)
            log_client.save_job_log(job_name, msg)
            if settings.EVENT_TASK_ENABLE:
                logger.info('add k8s event task to queue, event_task will handle it')
                event_task = EventTask(job_name=job_name, region_id=region_id, namespace=namespace,
                                    host=self.endpoint, token=self.token)
                event_task.save()

    def get_job(self, job_name, **kwargs):
        namespace = kwargs.get('namespace') or 'default'

        job = self._get_k8s_job_(job_name, namespace)
        pod_spec = job.spec.template.spec
        container_spec = pod_spec.containers[0]

        status = 'waiting'
        if job.status.succeeded:
            status = 'succeeded'
        elif job.status.failed:
            status = 'failed'
        elif job.status.active:
            status = 'running'

        temp_volumes, volumes = {}, []
        for m in container_spec.volume_mounts:
            temp_volumes[m.name] = {
                'container_path': m.mount_path,
                'mode': 'R' if m.read_only else 'RW',
            }
        for v in pod_spec.volumes:
            if v.name in temp_volumes:
                volumes.append({
                    'host_path': v.host_path.path,
                    'container_path': temp_volumes[v.name]['container_path'],
                    'mode': temp_volumes[v.name]['mode']
                })

        data = {
            'image': container_spec.image,
            'privileged': container_spec.security_context.privileged,
            'network': 'HOST' if pod_spec.host_network else 'BRIDGE',
            'command': container_spec.command or '',
            'cpu': container_spec.resources.limits['cpu'],
            'memory': container_spec.resources.limits['memory'].strip('Mi'),
            'volumes': volumes,
            'status': status
        }
        self._get_env_vars_from_containers_(container_spec, data)
        return data

    def delete_job(self, job_name, **kwargs):
        namespace = kwargs.get('namespace') or 'default'

        try:
            logger.debug("delete job {} from namespace {}".
                         format(job_name, namespace))
            self.client.delete_namespaced_job(
                namespace=namespace, name=job_name,
                body=V1DeleteOptions(grace_period_seconds=int(settings.GRACE_PERIOD_SECONDS),
                                     propagation_policy="Foreground"),
                **self._default_kwargs_)
        except ApiException as e:
            logger.debug('got k8s APIException: {}'.format(e))
            raise e
        except Exception as e:
            raise Exception('delete job in k8s failed: {}'.format(e))

    @property
    def _default_kwargs_(self):
        kwargs = {}
        if settings.REQUEST_TIMEOUT:
            kwargs['_request_timeout'] = settings.REQUEST_TIMEOUT
        return kwargs

    def _get_k8s_config_(self):
        config = Configuration()
        config.verify_ssl = False
        config.api_key_prefix = {'authorization': 'Bearer'}
        config.api_key = {'authorization': self.token}
        config.host = self.endpoint
        return config

    def _get_k8s_job_(self, job_name, namespace):
        try:
            job = self.client.read_namespaced_job(name=job_name, namespace=namespace)
            logger.debug('k8s job data:\n{}'.format(job))
            return job
        except ApiException as e:
            logger.debug('got k8s APIException: {}'.format(e))
            raise e
        except Exception as e:
            raise Exception('get job from k8s failed: {}'.format(e))

    def _get_k8s_pods(self, job_name, namespace):
        try:
            job = self._get_k8s_job_(job_name, namespace)
            match_labels = job.spec.selector.match_labels
            labels = 'controller-uid={}'.format(match_labels.get('controller-uid'))
            pods = self.pod_client.list_namespaced_pod(namespace, label_selector=labels)
            return pods
        except Exception as e:
            raise Exception('get pods from k8s failed: {}'.format(e))

    def _fake_mesos_task_id_(self, job_name):
        return 'ct:{}:0:{}:'.format(
            int(time.time() * 1000), job_name
        )

    def _convert_to_k8s_job_(self, data):
        volumes = []
        volume_mounts = []
        for i, v in enumerate(data.get('volumes', [])):
            volumes.append(
                V1Volume(name=str(i),
                         host_path=V1HostPathVolumeSource(v['host_path']))
            )
            volume_mounts.append(
                V1VolumeMount(
                    name=str(i),
                    mount_path=v['container_path'],
                    read_only=True if 'w' not in v['mode'].lower() else False
                )
            )

        container_spec = V1Container(
            name=data['name'],
            image_pull_policy='Always',
            image=data['image'],
            command=['/bin/sh', '-c', data['command']] if data.get('command') else [],
            resources=V1ResourceRequirements(
                limits={'cpu': data['cpu'], 'memory': '{}Mi'.format(data['memory'])},
                requests={'cpu': data['cpu'], 'memory': '{}Mi'.format(data['memory'])}),
            volume_mounts=volume_mounts,
            security_context=V1SecurityContext(privileged=data['privileged'])
        )
        self._add_env_vars_to_container_(container_spec, data)

        pod_template = V1PodTemplateSpec(
            metadata=V1ObjectMeta(name=data['name']),
            spec=V1PodSpec(
                restart_policy='Never',
                host_network=True if data['network'] == 'HOST' else False,
                containers=[container_spec],
                volumes=volumes
            )
        )

        return V1Job(
            api_version='batch/v1',
            kind='Job',
            metadata=V1ObjectMeta(name=data['name']),
            spec=V1JobSpec(template=pod_template)
        )

    def _get_env_vars_from_containers_(self, container_spec, data):
        if self.is_new_k8s:
            if container_spec.env:
                envs = []
                for e in container_spec.env:
                    if e.value_from:
                        envs.append(
                            {'name': e.name,
                             'valueFrom':
                                 {'configMapKeyRef': {
                                     'key': e.value_from.config_map_key_ref.key,
                                     'name': e.value_from.config_map_key_ref.name
                                 }}
                             }
                        )
                    else:
                        envs.append({'name': e.name, 'value': e.value})
                data['env'] = envs

            if container_spec.env_from:
                data['envFrom'] = \
                    [{'configMapRef': {'name': e.config_map_ref.name}}
                     for e in container_spec.env_from if e]
        else:
            data['environment_variables'] = \
                [{'name': e.name, 'value': e.value or ''} for e in container_spec.env if e]
        return data

    def _add_env_vars_to_container_(self, container, data):
        extra_env_vars = [
            {
                'name': 'mesos_task_id',
                'value': self._fake_mesos_task_id_(data['name'])
            }
        ]

        if self.is_new_k8s:
            env = data.get('env', [])
            environment_variables = data.get('environment_variables', [])
            env.extend(environment_variables)
            env.extend(extra_env_vars)

            env_vars = []
            for e in env:
                if "value" in e:
                    env_vars.append(V1EnvVar(name=e.get('name'), value=e.get('value', '')))
                elif "valueFrom" in e:
                    env_vars.append(V1EnvVar(name=e.get('name'), value_from=e.get('valueFrom')))
                else:
                    raise Exception('"value" or "valueFrom" is needed of env var: {}'.
                                    format(e.get('name')))
            container.env = env_vars

            env_from = data.get('envFrom', [])
            if env_from:
                container.env_from = [V1EnvFromSource(e.get('configMapRef')) for e in env_from]

        else:
            environment_variables = data.get('environment_variables', [])
            environment_variables.extend(extra_env_vars)

            env_vars = [V1EnvVar(e['name'], e['value']) for e in environment_variables]
            container.env = env_vars
        return container
