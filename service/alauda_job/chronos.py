import csv
import datetime
import logging
import time

import requests

from django.conf import settings

logger = logging.getLogger(__name__)


class ChronosClient():
    def __init__(self, endpoint, username=None, password=None):
        self.endpoint = endpoint
        self.username = username
        self.password = password

    def create_job(self, data, **kwargs):
        if data['privileged']:
            payload = self._constract_privileged_job(data)
        else:
            payload = self._constract_normal_job(data)
        logger.debug('Creating chronos job with data: {}'.format(payload))
        self._url_open('/scheduler/iso8601', 'post', json=payload)
        self._url_open('/scheduler/job/{}'.format(data['name']), 'put')

    def get_job(self, name, **kwargs):
        try:
            job_detail = self._url_open('/scheduler/jobs/search', 'get',
                                        params={'name': name}).json()[0]
        except IndexError:
            raise Exception('Job not found')
        jobs_graph = self._url_open('/scheduler/graph/csv', 'get').text.strip('\n')
        logger.debug('Job detail is:\n {}\n, Jobs graph is:\n {}\n'.format(
            job_detail, jobs_graph))
        last_status, current_status = None, None
        for row in csv.reader(jobs_graph.split('\n'), delimiter=','):
            if row[1] == name:
                last_status, current_status = row[2], row[3]
                break
        if last_status == 'success':
            status = 'succeeded'
        elif last_status == 'failure':
            status = 'failed'
        elif current_status == 'running':
            status = 'running'
        else:
            status = 'waiting'
        if 'container' in job_detail:
            return {
                'privileged': False,
                'image': job_detail['container']['image'],
                'network': job_detail['container']['network'],
                'command': job_detail['command'],
                'cpu': job_detail['cpus'],
                'memory': job_detail['mem'],
                'environment_variables': job_detail['environmentVariables'],
                'volumes': [
                    {
                        'container_path': v['containerPath'],
                        'host_path': v['hostPath'],
                        'mode': v['mode']
                    }
                    for v in job_detail['container']['volumes']
                    ],
                'status': status
            }
        else:
            return {
                'privileged': True,
                'command': job_detail['command'],
                'status': status
            }

    def delete_job(self, name, **kwargs):
        self._url_open('/scheduler/job/{}'.format(name), 'delete')

    def _constract_privileged_job(self, data):
        unix_timestamp_micro_seconds = int(time.time() * 1000)
        env_str = ''
        for item in data.get('environment_variables', []):
            env_str += ' -e {}={}'.format(item['name'], item['value'])

        vol_str = ''
        for item in data.get('volumes', []):
            vol_str += ' -v {}:{}:{}'.format(item['host_path'],
                                             item['container_path'],
                                             item['mode'].lower())

        command = ("docker run --privileged --cpu-shares {cpu} --memory {memory}M"
                   " -e mesos_task_id=ct:{timestamp}:0:{name}: {env_str}"
                   " {vol_str}"
                   " --net {network} --name {name}").format(
            cpu=int(data['cpu'] * 1024), memory=data['memory'],
            timestamp=unix_timestamp_micro_seconds, name=data['name'],
            env_str=env_str, vol_str=vol_str,
            network=data['network'].lower())
        if data.get('command'):
            command += ' --entrypoint /bin/sh {image} -c "{command}"'.format(
                image=data['image'], command=data['command'])
        else:
            command += ' {image}'.format(image=data['image'])

        return {
            'schedule': 'R0/{}/PT15M'.format(datetime.datetime.now().isoformat()),
            'name': data['name'],
            'command': command,
            'constraints': [],
            'epsilon': 'PT1H'
        }

    def _constract_normal_job(self, data):
        job = {
            'schedule': 'R0/{}/PT15M'.format(datetime.datetime.now().isoformat()),
            'command': data['command'] if data.get('command') else '',
            'name': data['name'],
            'ownerName': 'tresdin',
            'async': False,
            'environmentVariables': data.get('environment_variables', []),
            'epsilon': 'PT1H',
            'retries': 1,
            'constraints': [],
            'cpus': data['cpu'],
            'mem': data['memory'],
            'uris': ['file:///etc/.dockercfg'],
            'container': {
                'type': 'DOCKER',
                'forcePullImage': True,
                'image': data['image'],
                'network': data['network'],
                'volumes': [
                    {
                        'hostPath': v['host_path'],
                        'containerPath': v['container_path'],
                        'mode': v['mode']
                    }
                    for v in data.get('volumes', [])
                    ],
            }
        }
        return job

    def _url_open(self, path, method, check_response=True, **kwargs):
        uri = '{}/{}'.format(self.endpoint, path.lstrip('/'))
        if self.username and self.password:
            kwargs['auth'] = (self.username, self.password)
        if isinstance(kwargs.get('headers'), dict):
            headers = {'Content-Type': 'application/json'}
            headers.update(kwargs['headers'])
            kwargs['headers'] = headers
        else:
            kwargs['headers'] = {'Content-Type': 'application/json'}
        if settings.REQUEST_TIMEOUT:
            kwargs['timeout'] = settings.REQUEST_TIMEOUT

        request_func = getattr(requests, method)
        resp = request_func(uri, **kwargs)
        if not check_response:
            return resp
        if resp.status_code >= 300:
            logger.warning('Error response from chronos, status: {}, content: {}'
                           .format(resp.status_code, resp.text))
            raise Exception(resp.text)
        return resp
