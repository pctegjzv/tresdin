from .chronos import ChronosClient
from alauda_job.kubernetes import KubernetesClient


class AlaudaJob():
    def __init__(self, scheduler_config):
        if scheduler_config.scheduler_type == 'KUBERNETES':
            self._scheduler = KubernetesClient(scheduler_config.endpoint,
                                               token=scheduler_config.auth.get("token"),
                                               is_new_k8s=scheduler_config.is_new_k8s)
        elif scheduler_config.scheduler_type == 'CHRONOS':
            self._scheduler = ChronosClient(scheduler_config.endpoint, **scheduler_config.auth)
        else:
            raise Exception('Scheduler Type {} is not supported.'.
                            format(scheduler_config.scheduler_type))

    def create_job(self, data, **kwargs):
        return self._scheduler.create_job(data, **kwargs)

    def get_job(self, name, **kwargs):
        return self._scheduler.get_job(name, **kwargs)

    def delete_job(self, name, **kwargs):
        return self._scheduler.delete_job(name, **kwargs)
