import logging
from django.conf import settings
from mekansm.request import MekRequest

logger = logging.getLogger(__name__)


class MedusaRequest(MekRequest):
    endpoint = '{}/{}'.format(settings.MEDUSA_ENDPOINT, settings.MEDUSA_API_VERSION)
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'User-Agent': 'razzil/v1.0'
    }


CONFIGS = 'schedule_event_configs'


class MedusaClient(object):
    @classmethod
    def create_schedule_event_config(cls, rule, webhook_url):
        data = {
            "rule": rule,
            "webhook_url": webhook_url
        }
        return MedusaRequest.send(
            '{}/'.format(CONFIGS), method='POST', data=data)['data']

    @classmethod
    def get_schedule_event_config(cls, config_id):
        return MedusaRequest.send(
            '{}/{}'.format(CONFIGS, config_id),  method='GET')['data']

    @classmethod
    def get_schedule_event_config_list(cls, config_id_list):
        if not config_id_list:
            return []
        query_dict = {
            'config_ids': ','.join(config_id_list)
        }
        return MedusaRequest.send(
            '{}/'.format(CONFIGS), method='GET', params=query_dict)['data']

    @classmethod
    def delete_schedule_event_config(cls, config_id):
        MedusaRequest.send(
            '{}/{}'.format(CONFIGS, config_id), method='DELETE')

    @classmethod
    def update_schedule_event_config(cls, rule, config_id):
        data = {
            "rule": rule
        }
        MedusaRequest.send(
            '{}/{}'.format(CONFIGS, config_id), method='PUT', data=data)
