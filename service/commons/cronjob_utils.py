import logging
import time
from region import Region, SchedulerConfig
from django.conf import settings
from django.utils import timezone
from mekansm.utils import convert_to_unicode, is_string
from alauda_job.job_client import AlaudaJob
from jobs.models import (Job, STATUS_WAITING, STATUS_FAILED, COMPLETE_STATUS, STATUS_RUNNING)
from jobs.serializers import JobIncludeJobConfigSerializer
from jobs.env_client.env_client import EnvfileClient
from instant_jobs.serializers import InstantJobSerializer
from commons.medusa_client import MedusaClient
import json

LOG = logging.getLogger(__name__)
TIMEOUT = int(settings.DEFAULT_TIMEOUT) * 60


class CronjobUtil(object):

    @staticmethod
    def handle_job_list(config_uuid):
        LOG.info('handle_job_list: {}'.format(config_uuid))
        job_list = CronjobUtil.get_not_finished_jobs(config_uuid)
        job_list.reverse()
        CronjobUtil._handle_not_finished_jobs(job_list)

    @staticmethod
    def get_not_finished_jobs(config_uuid):
        query_set = Job.objects.filter(
            job_config__config_uuid=config_uuid).exclude(
                status__in=COMPLETE_STATUS)
        job_serializers = JobIncludeJobConfigSerializer(
            query_set, many=True)
        return job_serializers.data

    @staticmethod
    def _handle_not_finished_jobs(job_list):
        LOG.info('_handle_not_finished_jobs')
        job_queue = []
        for job in job_list:
            if CronjobUtil._remove_timeout_job(job):
                continue
            if Job.is_in_running_status(job['status']) == True or \
                    Job.is_waiting_status(job['status']) == True:
                job_queue = []
                break
            elif Job.is_blocked_status(job['status']):
                job_queue.append(job)

        for blocked_job in job_queue:
            if CronjobUtil._start_job_now(blocked_job):
                CronjobUtil._add_status_sync_scheduler(blocked_job['job_uuid'])
                break

    @staticmethod
    def _start_job_now(job):
        LOG.info('_start_job_now')
        db_job = Job.objects.get(job_uuid=job['job_uuid'])
        started = False
        try:
            CronjobUtil._create_cronjob(job)
        except Exception as e:
            db_job.status = STATUS_FAILED
            logging.error('start job failed: {}'.format(e))
        else:
            db_job.status = STATUS_WAITING
            started = True
        db_job.save()
        return started

    @staticmethod
    def _create_cronjob(job):
        LOG.info('_create_cronjob')
        scheduler, job_client = CronjobUtil.get_job_client(
            region_id=job['region_id'])
        job_data = CronjobUtil._constract_job_data(job)
        serializer = InstantJobSerializer(data=job_data)
        serializer.is_valid(raise_exception=True)
        valid_job_name = CronjobUtil.adjust_job_name(
            scheduler, serializer.validated_data['name'])
        serializer.validated_data['name'] = valid_job_name
        job_client.create_job(serializer.validated_data)

    @staticmethod
    def _constract_job_data(job):
        LOG.info('_constract_job_data')
        env_dict = CronjobUtil._constract_worker_container_env(job)
        job_data = {
            'name': 'alauda_work_{}'.format(job['job_uuid']),
            'image': settings.WORKER_IMAGE,
            'command': '/go/bin/techies job-container',
            'region_id': job['region_id'],
            'cpu': job['cpu'],
            'memory': job['memory'],
            'network': 'HOST',
            'environment_variables': CronjobUtil._worker_data_to_envvars(env_dict),
            'volumes': [
                {
                    'container_path': '/var/run/docker.sock',
                    'host_path': '/var/run/docker.sock',
                    'mode': 'RO'
                }
            ]
        }
        return job_data

    @staticmethod
    def _worker_data_to_envvars(env_dict):
        return [
            {'name': k.upper(), 'value': v}
            for k, v in CronjobUtil.format_dict_to_unicode(env_dict).items()
        ]

    @staticmethod
    def _constract_worker_container_env(job):
        if job['registry_project']:
            docker_image_tag = "{}/{}:{}".format(
                job['registry_project'],
                job['image_name'],
                job['image_tag'])
        else:
            docker_image_tag = "{}:{}".format(
                job['image_name'],
                job['image_tag'])
        user_data = {
            "full_username": job['created_by'],
            "token": job['user_token'],
            "namespace": job['namespace']
        }
        task_data = {
            "uuid": job['job_uuid'],
            "name": job['job_config']['config_name'],
            "registry_index": job['registry_index'],
            "docker_image_tag": docker_image_tag,
            "timeout": int(job['timeout']),
            "command": job['command'],
            "env_vars": CronjobUtil._get_job_envvar(job)
        }

        env_dict = {
            'jakiro_endpoint': settings.JAKIRO_API_ENDPOINT,
            'jakiro_api_version': settings.JAKIRO_API_VERSION,
            "region_id": job['region_id'],
            'user_data': json.dumps(user_data),
            'task_data': json.dumps(task_data),
            'debug': settings.TECHIES_DEBUG
        }
        return env_dict

    @staticmethod
    def _remove_timeout_job(job):
        started_time = int(time.mktime(
            time.strptime(job['created_at'], "%Y-%m-%dT%H:%M:%S.%f")))

        seconds = time.time() - started_time
        if seconds > TIMEOUT:
            CronjobUtil._stop_time_out_job(job)
            return True
        else:
            return False

    @staticmethod
    def _stop_time_out_job(job):
        if job['status'] in [STATUS_WAITING, STATUS_RUNNING]:
            CronjobUtil.delete_cronjob(job)
        Job.objects.filter(job_uuid=job['job_uuid']).update(
            status=STATUS_FAILED, ended_at=timezone.now())

    @staticmethod
    def delete_cronjob(job):
        if not isinstance(job, dict):
            raise Exception('Invalid parameter. The parameter should be the dict type')
        try:
            scheduler, job_client = CronjobUtil.get_job_client(
                region_id=job['region_id'])
            job_name = CronjobUtil.adjust_job_name(
                scheduler, 'alauda_work_{}'.format(job['job_uuid']))
            if CronjobUtil.is_job_exist(job['region_id'], job_name):
                job_client.delete_job(job_name)
        except Exception as e:
            logging.error('error: {}'.format(e))

    @staticmethod
    def format_dict_to_unicode(input_dict):
        for key, value in input_dict.items():
            if isinstance(value, dict):
                input_dict[key] = CronjobUtil.format_dict_to_unicode(value)
            elif value is None:
                input_dict[key] = ''
            else:
                if is_string(value):
                    input_dict[key] = convert_to_unicode(value)
                else:
                    input_dict[key] = str(value)
        return input_dict

    @staticmethod
    def remove_jobs(config_uuid):
        query_set = Job.objects.filter(
            job_config__config_uuid=config_uuid)
        if query_set.exists():
            job_list = []
            for job in query_set:
                if job.status == STATUS_WAITING:
                    job_serializer = JobIncludeJobConfigSerializer(job)
                    CronjobUtil.delete_cronjob(job_serializer.data)
                else:
                    job_list.append(job.job_uuid)
            Job.objects.filter(job_uuid__in=job_list).delete()

    @staticmethod
    def get_job_client(region_id=None):
        if not region_id:
            scheduler_type = settings.DEFAULT_JOB_SCHEDULER_TYPE
            endpoint = settings.DEFAULT_JOB_SCHEDULER_ENDPOINT
            if scheduler_type == 'KUBERNETES':
                auth = {
                    'token': settings.DEFAULT_JOB_SCHEDULER_TOKEN
                }
            else:
                auth = {
                    'username': settings.DEFAULT_JOB_SCHEDULER_USERNAME,
                    'password': settings.DEFAULT_JOB_SCHEDULER_PASSWORD
                }
            c = {
                'scheduler_type': scheduler_type,
                'endpoint': endpoint,
                'auth': auth
            }
            scheduler_config = SchedulerConfig(**c)
        else:
            region = Region(region_id)
            scheduler_config = region.get_job_scheduler_config()
        return scheduler_config, AlaudaJob(scheduler_config)

    @staticmethod
    def _get_job_envvar(job):
        LOG.info('_get_job_envvar')
        envs = {}
        envvars = job.get('envvars', [])
        for env in envvars:
            if env['name'].strip():
                envs[env['name']] = env['value']
        for envfile in job.get('envfiles', {}):
            result = EnvfileClient.get_envfile(
                envfile_uuid=envfile.get('uuid'))
            for name, value in result.get('content'):
                if name.strip():
                    envs[name] = value
        return envs

    @staticmethod
    def adjust_job_name(scheduler, job_name):
        if scheduler.scheduler_type == 'KUBERNETES':
            for keyword in ['private_build_', 'alauda_work_', 'cd_task_']:
                if job_name.startswith(keyword):
                    return keyword.replace('_', '-') + job_name[len(keyword):]
        return job_name

    @staticmethod
    def parse_username(data):
        if data.get('created_by') and '/' in data['created_by']:
            return data['created_by'].split('/')[1]
        return data.get('created_by')

    @staticmethod
    def is_job_exist(region_id, job_name):
        _, job_client = CronjobUtil.get_job_client(region_id)
        try:
            job_client.get_job(job_name)
        except:
            return False
        return True

    @staticmethod
    def get_job_info(job):
        if not isinstance(job, dict):
            raise Exception('Invalid parameter. The parameter should be the dict type')
        job_info = {}
        scheduler, job_client = CronjobUtil.get_job_client(
            region_id=job['region_id'])
        job_name = CronjobUtil.adjust_job_name(
            scheduler, 'alauda_work_{}'.format(job['job_uuid']))
        if CronjobUtil.is_job_exist(job['region_id'], job_name):
            job_info = job_client.get_job(job_name)
        return job_info

    @staticmethod
    def _add_status_sync_scheduler(job_uuid):
        sync_url = CronjobUtil._get_job_status_sync_url(job_uuid)
        result = MedusaClient.create_schedule_event_config(
            settings.STATUS_SYNC_RULE,
            sync_url)
        LOG.info("add sync scheduler to medusa, job_uuid ={}, result: {}".format(
            job_uuid, result))

    @staticmethod
    def _get_job_status_sync_url(job_uuid):
        return '#ALAUDA_TRESDIN_ENDPOINT#/v1/jobs/{}/sync_status/'.format(job_uuid)
