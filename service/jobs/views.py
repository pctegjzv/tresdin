import logging
import time
import json

from django.conf import settings
from rest_framework import viewsets, status
from rest_framework.response import Response
from django.shortcuts import get_object_or_404, Http404
from django.db import transaction
from django.utils import timezone

from alauda.tracing import tracing_response_time

from jobs.models import Job
from job_configs.models import JobConfig
from jobs.serializers import JobListSerializer, JobUpdateSerializer, \
                             JobCreateSerializer, JobIncludeJobConfigSerializer, \
                             JobStatusSerializer
from jobs.models import STATUS_BLOCKED, \
    STATUS_RUNNING, STATUS_STOPPED, COMPLETE_STATUS, STATUS_WAITING, STATUS_FAILED, \
    STATUS_SUCCEEDED
from commons.cronjob_utils import CronjobUtil
from jobs.log_client.log_client import ElasticSearchClient
from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger

from jobs.client import FurionClient

logger = logging.getLogger(__name__)
TIMESPAN_LIMIT = 60 * 60 * 24 * 3


class JobViewset(viewsets.ViewSet):
    jobs_query_set = Job.objects.all()
    jobs_config_query_set = JobConfig.objects.all()
    job_client = None
    scheduler=None

    @tracing_response_time()
    def start_job(self, request):
        logger.info('start a job with : {}'.format(request.data))
        config_uuid = request.data.get('config_uuid')

        job_data = self._constract_job_data(request.data)

        job_serializer = JobCreateSerializer(data=job_data)
        job_serializer.is_valid(raise_exception=True)
        job_serializer.save()

        with transaction.atomic():
            CronjobUtil.handle_job_list(config_uuid=config_uuid)
        return Response(data={'job_uuid': job_serializer.data.get('job_uuid')},
                        status=status.HTTP_201_CREATED)

    @tracing_response_time()
    def get_job(self, request, job_uuid):
        logger.info('get_job')
        job = get_object_or_404(self.jobs_query_set,
                                job_uuid=job_uuid)
        job_serializer = JobIncludeJobConfigSerializer(job)
        result = self._constract_job_response(job_serializer.data)
        return Response(result, status=status.HTTP_200_OK)

    @tracing_response_time()
    def list_jobs(self, request):
        logger.info('list_jobs')

        page_size = request.data.get('page_size')
        if not page_size:
            page_size = settings.LIST_PAGE_SIZE
        page = request.data.get('page')
        if not page:
            page = settings.LIST_PAGE

        uuids = request.data.get('uuids', '').split(',')
        job_set = Job.objects.filter(job_config__config_uuid__in=uuids)
        job_serializers = JobListSerializer(
            job_set, many=True)

        page_data = self._list_jobs(job_serializers.data, page, page_size)

        return Response(data=page_data, status=status.HTTP_200_OK)

    @tracing_response_time()
    def stop_job(self, request, job_uuid):
        logger.info('stop_job')
        job = get_object_or_404(self.jobs_query_set,
                                job_uuid=job_uuid)

        if job.status in [STATUS_WAITING, STATUS_RUNNING]:
            job_serializer = JobIncludeJobConfigSerializer(job)
            CronjobUtil.delete_cronjob(job_serializer.data)
        job.status = STATUS_FAILED
        job.save()

        job_config = self._update_last_job_status(job.job_config.config_uuid)

        with transaction.atomic():
            CronjobUtil.handle_job_list(config_uuid=job_config.config_uuid)
        return Response(data={'job_uuid': job_uuid}, status=status.HTTP_200_OK)

    @tracing_response_time()
    def delete_job(self, request, job_uuid):
        job = get_object_or_404(self.jobs_query_set,
                                job_uuid=job_uuid)
        if job.status in [STATUS_WAITING, STATUS_RUNNING]:
            job_serializer = JobIncludeJobConfigSerializer(job)
            CronjobUtil.delete_cronjob(job_serializer.data)

        config_uuid = job.job_config.config_uuid
        job.delete()

        job_config = self._update_last_job_status(config_uuid)

        with transaction.atomic():
            CronjobUtil.handle_job_list(config_uuid=job_config.config_uuid)
        return Response(data={'job_uuid': job_uuid},
                        status=status.HTTP_200_OK)

    @tracing_response_time()
    def update_job_status(self, request, job_uuid):
        logger.info('update_job_status')
        if not request.data.get('status'):
            raise Exception('param is invalid')
        request.data['status'] = request.data.get('status', '').upper()

        job_set = Job.objects.filter(job_uuid=job_uuid)
        if job_set.exists():
            job = job_set.get()
            if request.data['status'] == "ONGOING":
                request.data['status'] = STATUS_RUNNING
                request.data['started_at'] = timezone.now()
            elif request.data['status'] in COMPLETE_STATUS:
                job_serializer = JobIncludeJobConfigSerializer(job)
                CronjobUtil.delete_cronjob(job_serializer.data)
                request.data['ended_at'] = timezone.now()

            if job.status != request.data.get('status'):
                job_serializer = JobUpdateSerializer(job, data=request.data)
                job_serializer.is_valid(raise_exception=True)
                job_serializer.save()

                job_config = self._update_last_job_status(job.job_config.config_uuid)

                if job_config:
                    with transaction.atomic():
                        CronjobUtil.handle_job_list(config_uuid=job_config.config_uuid)
        return Response(data={'job_uuid': job_uuid},
                        status=status.HTTP_200_OK)

    @tracing_response_time()
    def get_job_status(self, request, job_uuid):
        logger.info('get_job_status')
        result = {}
        job_set = Job.objects.filter(job_uuid=job_uuid)
        if not job_set.exists():
            result['status'] = STATUS_FAILED.lower()
            result['job_uuid'] = job_uuid
        else:
            job_serializer = JobStatusSerializer(job_set.get())
            result = job_serializer.data
            result['status'] = job_serializer.data['status'].lower()
        return Response(result, status.HTTP_200_OK)

    @tracing_response_time()
    def get_job_logs(self, request, job_uuid):
        logger.info('get_job_logs')
        job = get_object_or_404(self.jobs_query_set, job_uuid=job_uuid)
        start_time = request.query_params.get('start_time')
        end_time = request.query_params.get('end_time')
        limit = int(request.query_params.get('limit', 2000))
        if not start_time and not end_time:
            start_time = time.mktime(job.started_at.timetuple()) - 3600
            end_time = start_time + TIMESPAN_LIMIT
        else:
            if not start_time:
                start_time = end_time - TIMESPAN_LIMIT
            elif not end_time:
                end_time = start_time + TIMESPAN_LIMIT

        job_uuid = str(job_uuid).replace('-', '_')
        logger.info(job_uuid)

        data = FurionClient.get_log_source(job.region_id)['data']['read_log_source']
        if len(data) == 0 or data[0]['type'] == 'default':
            client = ElasticSearchClient()
        else:
            client = ElasticSearchClient(data[0].get('query_address', None),
                                         data[0].get('username', None),
                                         data[0].get('password', None))

        _, logs_list = client.get_job_log(job_id=job_uuid,
                                          start_time=int(start_time),
                                          end_time=int(end_time),
                                          limit=limit)
        return Response(data=logs_list, status=status.HTTP_200_OK)

    @tracing_response_time()
    def sync_job_status(self, request, job_uuid):
        try:
            job = get_object_or_404(self.jobs_query_set, job_uuid=job_uuid)
            job_serializers = JobIncludeJobConfigSerializer(job)
            job_info = CronjobUtil.get_job_info(job_serializers.data)
            job_status = job_info.get('status')
            if (not job_status) and (job.status not in COMPLETE_STATUS):
                logger.error(
                    'sync_job_status is failed , try {}, job_status: {}'.format(str(job.status_sync_fail_count),
                                                                                job_status))
                job.status_sync_fail_count += 1
                job.save()
                if job.status_sync_fail_count > int(settings.SYNC_COUNT):
                    job.status = STATUS_FAILED
                    job.save()
                    return Response('delete job sync', status=status.HTTP_410_GONE)
            if job_status and job_status.upper() != job.status and job.status != STATUS_STOPPED:
                job.status = job_status.upper()

                if job.status == STATUS_RUNNING:
                    job.started_at = timezone.now()
                job.save()
                self._update_last_job_status(job.job_config.config_uuid)

            if job.status in COMPLETE_STATUS:
                logger.info('job is completed: status is {}'.format(job.status))
                if not job.started_at:
                    logger.info('start time is None, so the start time is set to create time')
                    job.started_at = job.created_at
                job.ended_at = timezone.now()
                job.save()
                self._update_last_job_status(job.job_config.config_uuid)

                CronjobUtil.delete_cronjob(job_serializers.data)
                with transaction.atomic():
                    CronjobUtil.handle_job_list(config_uuid=job.job_config.config_uuid)
                return Response('delete job sync', status=status.HTTP_410_GONE)

        except Http404 as e:
            logger.error('job history is already deleted: {}'.format(e))
            return Response('delete job sync', status=status.HTTP_410_GONE)
        except Exception as e:
            logger.error('sync_job_status is failed , try {}, err_info: {}'.format(str(job.status_sync_fail_count), e))
            job.status_sync_fail_count += 1
            job.save()
            if job.status_sync_fail_count > int(settings.SYNC_COUNT):
                job.status = STATUS_FAILED
                job.save()
                return Response('delete job sync', status=status.HTTP_410_GONE)
        with transaction.atomic():
            CronjobUtil.handle_job_list(config_uuid=job.job_config.config_uuid)
        return Response('update job sync', status=status.HTTP_200_OK)

    def _constract_job_data(self, data):
        job_config = get_object_or_404(self.jobs_config_query_set,
                                       config_uuid=data.get('config_uuid'))
        logger.debug(job_config.config_uuid)
        job_data = {
            'job_config': job_config.config_uuid,
            'status': STATUS_BLOCKED,
            'space_uuid': job_config.space_uuid,
            'region_id': job_config.region_id,
            'command_type': job_config.command_type,
            'command': job_config.command,
            'timeout': job_config.timeout,
            'cpu': job_config.cpu,
            'memory': job_config.memory,
            'registry_index': job_config.registry_index,
            'registry_project': job_config.registry_project,
            'registry_name': job_config.registry_name,
            'image_name': job_config.image_name,
            'image_tag': job_config.image_tag,
            'envvars': json.dumps(job_config.envvars),
            'envfiles': json.dumps(job_config.envfiles),
            'user_token': data.get('user_token'),
            'namespace': data.get('namespace'),
            'created_by': data.get('created_by')
        }
        if 'is_auto_scheduled' in data:
            job_data['is_auto_scheduled'] = data['is_auto_scheduled']
        logger.debug(job_data)
        return job_data

    def _list_jobs(self, job_serializers_set, page, page_size):
        paginator = Paginator(job_serializers_set, page_size)
        try:
            jobs = paginator.page(page)
        except PageNotAnInteger:
            jobs = paginator.page(1)
        except EmptyPage:
            jobs = paginator.page(paginator.num_pages)

        data_page = {}
        for job in jobs.object_list:
            job['config_uuid'] = job['job_config']['config_uuid']
            job['config_name'] = job['job_config']['config_name']
            job['project_uuid'] = job['job_config']['project_uuid']
            job['created_by'] = CronjobUtil.parse_username(job)
            del job['job_config']

        data_page['count'] = len(job_serializers_set)
        data_page['page_sizes'] = page_size
        if jobs.has_previous():
            data_page['previous'] = jobs.previous_page_number()
        else:
            data_page['previous'] = None
        if jobs.has_next():
            data_page['next'] = jobs.next_page_number()
        else:
            data_page['next'] = None
        data_page['results'] = jobs.object_list
        return data_page

    def _constract_job_response(self, data):
        data['config_uuid'] = data['job_config']['config_uuid']
        data['config_name'] = data['job_config']['config_name']
        data['project_uuid'] = data['job_config']['project_uuid']
        data['created_by'] = CronjobUtil.parse_username(data)
        del data['job_config']
        return data

    def _update_last_job_status(self, config_uuid):
        last_job = Job.objects.filter(job_config__config_uuid=config_uuid).\
            filter(status__in=[STATUS_RUNNING, STATUS_FAILED,
                               STATUS_STOPPED, STATUS_SUCCEEDED]).order_by("-created_at")[:1]

        job_config = get_object_or_404(self.jobs_config_query_set,
                                       config_uuid=config_uuid)
        if last_job:
            job_config.last_job['job_uuid'] = last_job[0].job_uuid
            job_config.last_job['created_at'] = last_job[0].created_at
            job_config.last_job['status'] = last_job[0].status
            job_config.last_job['started_at'] = last_job[0].started_at
            job_config.last_job['ended_at'] = last_job[0].ended_at
        else:
            job_config.last_job={}
        job_config.save()
        return job_config


