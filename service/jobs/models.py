import uuid
from jsonfield import JSONField
from django.db import models
from job_configs.models import JobConfig, ENTRYPOINT_TYPE, DEFAULT_COMMAND

STATUS_BLOCKED = 'BLOCKED'
STATUS_WAITING = 'WAITING'
STATUS_RUNNING = 'RUNNING'
STATUS_SUCCEEDED = 'SUCCEEDED'
STATUS_FAILED = 'FAILED'
STATUS_STOPPED = 'STOPPED'

STATUS_TYPE = (
    (STATUS_BLOCKED, STATUS_BLOCKED),
    (STATUS_WAITING, STATUS_WAITING),
    (STATUS_RUNNING, STATUS_RUNNING),
    (STATUS_SUCCEEDED, STATUS_SUCCEEDED),
    (STATUS_FAILED, STATUS_FAILED),
    (STATUS_STOPPED, STATUS_STOPPED)
)

COMPLETE_STATUS = [STATUS_FAILED, STATUS_SUCCEEDED, STATUS_STOPPED]


class Job(models.Model):
    job_uuid = models.CharField(max_length=36,
                                primary_key=True,
                                default=uuid.uuid4)
    job_config = models.ForeignKey(JobConfig,
                                   related_name='jobs',
                                   on_delete=models.CASCADE)
    namespace = models.CharField(max_length=64)
    user_token = models.CharField(max_length=100)
    status = models.CharField(max_length=64,
                              choices=STATUS_TYPE,
                              default=STATUS_BLOCKED,
                              blank=True)
    is_auto_scheduled = models.BooleanField(blank=True, default=False)
    space_uuid = models.CharField(max_length=64, blank=True, default='')
    region_id = models.CharField(max_length=64)
    command_type = models.CharField(max_length=64,
                                    choices=ENTRYPOINT_TYPE,
                                    default=DEFAULT_COMMAND)
    command = models.CharField(max_length=128,
                               blank=True,
                               default='')
    timeout = models.FloatField(default=1440.0,
                                blank=True)
    cpu = models.FloatField(blank=True, default=0.5)
    memory = models.IntegerField(blank=True, default=512)

    registry_index = models.CharField(max_length=64, blank=True, default='')
    registry_name = models.CharField(max_length=64, blank=True, default='')
    registry_project = models.CharField(max_length=64, blank=True, default='')
    image_name = models.CharField(max_length=64)
    image_tag = models.CharField(max_length=64, blank=True, default='latest')
    envvars = JSONField(blank=True, default=[])
    envfiles = JSONField(blank=True, default=[])

    status_sync_fail_count = models.SmallIntegerField(default=0, blank=True, null=True)
    created_by = models.CharField(max_length=30)
    created_at = models.DateTimeField(auto_now_add=True)
    started_at = models.DateTimeField(null=True, blank=True)
    ended_at = models.DateTimeField(null=True, blank=True)

    class Meta:
        ordering = ('-created_at',)
        db_table = 'tresdin_jobs_job'

    def __str__(self):
        return self.job_uuid

    @staticmethod
    def is_in_running_status(job_status):
        if job_status == STATUS_RUNNING:
            return True
        else:
            return False

    @staticmethod
    def is_waiting_status(job_status):
        if job_status == STATUS_WAITING:
            return True
        else:
            return False

    @staticmethod
    def is_blocked_status(job_status):
        if job_status == STATUS_BLOCKED:
            return True
        else:
            return False
