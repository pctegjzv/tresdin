import datetime
import logging
import time
import pytz

from elasticsearch import Elasticsearch
from django.conf import settings

from jobs.client import FurionClient

LOG = logging.getLogger(__name__)


class ElasticSearchClient(object):
    def __init__(self, host=None, username=None, password=None):
        host = host or settings.LOGGING_STORAGE['host']
        username = username or settings.LOGGING_STORAGE['username']
        password = password or settings.LOGGING_STORAGE['password']
        LOG.info('es connection host is {}'.format(host))
        if username and password:
            self.conn = Elasticsearch([host], http_auth=(username, password))
        else:
            self.conn = Elasticsearch([host])
    
    def save_job_log(self, job_name, message, log_level=0):
        """
        Save build log to elasticsearch.
        this method will refactor when tiny refactored(release-1.11)

        :param job_name: job_name in tresdin form as private-build-buildID
        :type job_name: str
        :param message: log message
        :type message: str
        :param log_level: log level
        :type log_level: int
        """
        now_time = get_now_cur_time_zone()
        app_id = job_name.replace('-', '_')[len('private-build-'):]
        index = 'log' + '-%04d%02d%02d' % (now_time.year,
                                            now_time.month,
                                            now_time.day)
        doc = {
            'time': int(time.time()*1000000),
            'machine': '',
            'app_id': app_id,
            'instance_id': app_id,
            'container_name': '',
            'log_data': message,
            'log_type': 'stdout',
            'log_level': log_level,
            'filename': 'stdout',
            'service_name': '',
            'region_name': '',
            'region_id': '',
            'instance_id8': ''
        }

        return self.save_app_log(index=index, doc_type='log', body=doc)

    def save_app_log(self, **kwargs):
        ret = None
        try:
            ret = self.conn.index(**kwargs)
        except Exception as e:
            LOG.error('save log {} error {}'.format(kwargs.get('body', ''), e))
        return ret

    def get_job_log(self, job_id, start_time, end_time, limit=2000):
        job_id = job_id.replace('-', '_')
        size = limit or 2000
        fields = ['time', 'log_data', 'log_level']
        index_list = self._get_index(start_time, end_time)
        result_list, return_list, result = [], [], ''
        if index_list is None:
            return result, return_list
        for l in index_list:
            start, end, index = l['start'], l['end'], l['index']
            if start == end:
                continue

            must = [
                {
                    'term': {'app_id': job_id}
                },
                {
                    'range': {
                        'time': {'gte': start * 1000000, 'lt': end * 1000000}
                    }
                }
            ]

            body = {
                'sort': {'time': {'order': 'desc'}},
                'fields': fields,
                'query': {'bool': {'must': must}}
            }
            try:
                res = self.conn.search(size=size,
                                       doc_type='log',
                                       index=index,
                                       body=body)
            except Exception as ex:
                LOG.warn('get log fail! error: {}'.format(str(ex)))
                continue
            if res['hits']['total'] == 0:
                continue

            result_list.append(res)
            size = size - res['hits']['total']
            if size <= 0:
                break

        result_list.reverse()
        for res in result_list:
            res['hits']['hits'].reverse()
            for log in res['hits']['hits']:
                log_info = log['fields']
                try:
                    log_level = log_info['log_level'][0]
                except KeyError:
                    log_level = 'INFO'

                result += log_info['log_data'][0] + '\r\n'
                return_list.append({
                    'time': log_info['time'][0] / 1000000,
                    'message': log_info['log_data'][0],
                    'level': log_level
                })

        return result, return_list

    def _get_index(self, start_time, end_time, index_type='log'):
        if start_time >= end_time:
            return None
        result_list = []
        start_datetime = datetime.datetime.fromtimestamp(start_time)
        end_datetime = datetime.datetime.fromtimestamp(end_time)

        index = index_type + '-%04d%02d%02d' % (start_datetime.year,
                                                start_datetime.month,
                                                start_datetime.day)
        start = start_time

        next_day, time_stamp = self._get_next_day(start_datetime)
        next_day = start_datetime

        while next_day.date() < end_datetime.date():
            l = {
                'index': index,
                'start': start,
                'end': time_stamp
            }
            result_list.append(l)
            start = time_stamp
            next_day, time_stamp = self._get_next_day(next_day)
            index = index_type + '-%04d%02d%02d' % (next_day.year,
                                                    next_day.month,
                                                    next_day.day)
            time_stamp += 24 * 3600

        l = {
            'index': index,
            'start': start,
            'end': end_time
        }
        result_list.append(l)
        result_list.reverse()
        return result_list

    def _get_next_day(self, src):
        if not isinstance(src, datetime.date):
            return None
        td = datetime.datetime(year=src.year,
                               month=src.month,
                               day=src.day,
                               hour=23,
                               minute=59,
                               second=59)
        time_stamp = time.mktime(td.timetuple())
        time_stamp += 1

        next_day = datetime.datetime.fromtimestamp(time_stamp)
        return next_day, int(time_stamp)

def get_es_client(region_id):
    """
    Get elasticsearch client by region id

    :param region_id: region id
    :type region_id: str
    :returns: es client
    :rtype: jobs.log_client.log_client.ElasticSearchClient
    """

    if not region_id:
        # if cluster is public, It does not belong to any region.
        # and cluster will not maintained by furion
        LOG.debug('region id is none when get es client, use default setting')
        return ElasticSearchClient()
        
    try:
        data = FurionClient.get_log_source(region_id)['data']['read_log_source']
    except Exception as e:
        msg = 'get_log_source info by FurionClient with region_id {} error {}'.format(region_id, e)
        LOG.error(msg)
        raise

    if len(data) == 0 or data[0]['type'] == 'default':
        client = ElasticSearchClient()
    else:
        client = ElasticSearchClient(data[0].get('query_address', None),
                                     data[0].get('username', None),
                                     data[0].get('password', None))
    return client

def get_now_cur_time_zone():
    """Get time by timezone
    """
    now = datetime.datetime.now()
    if settings.CUR_TIME_ZONE != settings.TIME_ZONE:
        now = datetime.datetime.now(pytz.timezone(settings.CUR_TIME_ZONE))
    return now