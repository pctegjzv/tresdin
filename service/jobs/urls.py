from django.conf.urls  import url
from jobs.views import JobViewset
from django.conf import settings


urlpatterns = [
    url(r'^/?$',
        JobViewset.as_view(
            {
                'post': 'start_job',
                'get': 'list_jobs'
            }
        )),
    url(r'^/(?P<job_uuid>{})/?$'.format(settings.UUID_PATTERN),
        JobViewset.as_view(
            {
                'get': 'get_job',
                'put': 'stop_job',
                'delete': 'delete_job'
            }
        )),
    url(r'^/(?P<job_uuid>{})/logs/?$'.format(settings.UUID_PATTERN),
        JobViewset.as_view(
            {
                'get': 'get_job_logs'
            }
        )),
    url(r'^/(?P<job_uuid>{})/status/?$'.format(settings.UUID_PATTERN),
        JobViewset.as_view(
            {
                'get': 'get_job_status',
                'put': 'update_job_status'
            }
        )),
    url(r'^/(?P<job_uuid>{})/sync_status/?$'.format(settings.UUID_PATTERN),
        JobViewset.as_view(
            {
                'post': 'sync_job_status'
            }
        ))
]
