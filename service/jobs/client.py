import logging
from django.conf import settings
from mekansm.request import MekRequest
logger = logging.getLogger(__name__)


class FurionClient(object):

    class FurionRequest(MekRequest):
        endpoint = '{}/v1'.format(settings.FURION_ENDPOINT)

    @classmethod
    def get_log_source(cls, region_id):
        return cls.FurionRequest.send('regions/{}/log_sources'.format(region_id), method="GET",
                                      timeout=3)
