import logging
from django.conf import settings
from mekansm.request import MekRequest

logger = logging.getLogger(__name__)


class EnvfileRequest(MekRequest):
    endpoint = '{}/{}'.format(settings.SVEN_ENDPOINT, settings.SVEN_API_VERSION)
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'User-Agent': 'razzil/v1.0'
    }


ENVFILE = 'env-file'


class EnvfileClient():
    @classmethod
    def get_envfile(cls, envfile_uuid):
        return EnvfileRequest.send(path='{}/{}'.format(ENVFILE,envfile_uuid.lstrip()),
                                   method='GET')['data']
