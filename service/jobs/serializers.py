from job_configs.serializers import JobConfigSerializer
from rest_framework import serializers
from jobs.models import Job


class JobSerializer(serializers.ModelSerializer):
    envvars = serializers.JSONField(required=False)
    envfiles = serializers.JSONField(required=False)

    class Meta:
        model = Job
        read_only_fields = ('job_uuid', 'created_at')
        fields='__all__'


class JobStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Job
        fields = ('job_uuid', 'status')


class JobCreateSerializer(serializers.ModelSerializer):
    envvars = serializers.JSONField(required=False)
    envfiles = serializers.JSONField(required=False)

    class Meta:
        model = Job
        fields = '__all__'


class JobIncludeJobConfigSerializer(serializers.ModelSerializer):
    envvars = serializers.JSONField(required=False)
    envfiles = serializers.JSONField(required=False)
    job_config = JobConfigSerializer()

    class Meta:
        model = Job
        fields = '__all__'


class JobUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Job
        fields = ('status', 'started_at', 'ended_at')


class JobListSerializer(serializers.ModelSerializer):
    job_config = JobConfigSerializer()

    class Meta:
        model = Job
        exclude = ('cpu', 'memory', 'envvars', 'envfiles', 'user_token')
