from rest_framework import serializers
from job_configs.models import JobConfig


class JobConfigSerializer(serializers.ModelSerializer):
    envvars = serializers.JSONField(required=False)
    envfiles = serializers.JSONField(required=False)
    last_job = serializers.JSONField(required=False)
    project_uuid = serializers.CharField(required=False, allow_blank=True)
    space_uuid = serializers.CharField(required=False, allow_blank=True)

    class Meta:
        model = JobConfig
        fields = '__all__'
        read_only_fields = ('created_at', 'updated_at')


class JobConfigForUpdateSerializer(serializers.ModelSerializer):
    envvars = serializers.JSONField(required=False)
    envfiles = serializers.JSONField(required=False)
    last_job = serializers.JSONField(required=False)
    timeout = serializers.FloatField(required=False)

    class Meta:
        model = JobConfig
        exclude = ('config_uuid', 'config_name', 'project_uuid', 'space_uuid',
                   'created_by', 'created_at', 'updated_at')


class JobConfigListSerializer(serializers.ModelSerializer):
    last_job = serializers.JSONField(required=False)

    class Meta:
        model = JobConfig
        exclude = ('envvars', 'envfiles', 'schedule_config_id', 'schedule_config_secret_key')
