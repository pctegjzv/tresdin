# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-06-09 10:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('job_configs', '0002_auto_20170609_1009'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jobconfig',
            name='timeout',
            field=models.FloatField(blank=True, default=3600.0),
        ),
    ]
