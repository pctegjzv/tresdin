from django.conf.urls import url
from job_configs.views import JobConfigViewset
from django.conf import settings


urlpatterns = [
    url(r'^/?$',
        JobConfigViewset.as_view(
            {
                'get': 'list',
                'post': 'create'
            }
        )),
    url(r'^/(?P<config_uuid>{})/?$'.format(settings.UUID_PATTERN),
        JobConfigViewset.as_view(
            {
                'get': 'retrieve',
                'put': 'update',
                'delete': 'delete'
            }
        ))
]