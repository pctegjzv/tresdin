import json
import uuid
import logging

from rest_framework import viewsets, status
from rest_framework.response import Response
from django.db import transaction

from alauda.tracing import tracing_response_time

from job_configs.models import JobConfig
from jobs.models import Job
from commons.medusa_client import MedusaClient
from django.shortcuts import get_object_or_404
from job_configs.serializers import JobConfigForUpdateSerializer, \
    JobConfigSerializer, JobConfigListSerializer
from commons.cronjob_utils import CronjobUtil
from django.conf import settings

LOG = logging.getLogger(__name__)


class JobConfigViewset(viewsets.ViewSet):
    job_config_query_set = JobConfig.objects.all()
    job_query_set = Job.objects.all()

    @tracing_response_time()
    def list(self, request):
        LOG.info('list')
        uuids = request.data.get('uuids', '').split(',')
        job_config_set = JobConfig.objects.filter(config_uuid__in=uuids)

        job_config_serializer = JobConfigListSerializer(
            job_config_set, many=True)
        for job_config in job_config_serializer.data:
            job_config['created_by'] = CronjobUtil.parse_username(job_config)
        LOG.debug('list job config: {}'.format(job_config_serializer.data))

        return Response(data=job_config_serializer.data,
                        status=status.HTTP_200_OK)

    @tracing_response_time()
    def retrieve(self, request, config_uuid):
        job_config = get_object_or_404(self.job_config_query_set,
                                       config_uuid=config_uuid)
        serializer = JobConfigSerializer(job_config)
        result = serializer.data
        result['created_by'] = CronjobUtil.parse_username(result)
        return Response(result, status=status.HTTP_200_OK)

    @tracing_response_time()
    def create(self, request):
        LOG.info('create a job config with : ' + json.dumps(request.data))
        self.validate_data(request.data)

        config_uuid = request.data['config_uuid'] = str(uuid.uuid4())
        # if has schedule config
        if request.data.get('schedule_rule'):
            rule = request.data.get('schedule_rule')
            job_callback_url = self._get_schedule_job_url(config_uuid)
            LOG.info('job_callback_url is {}'.format(job_callback_url))
            result = MedusaClient.create_schedule_event_config(
                rule, job_callback_url)
            request.data['schedule_config_id'] = result.get('id')
            request.data['schedule_config_secret_key'] = result.get('secret_key')

        if request.data.get('envvars'):
            request.data['envvars'] = self._standardize_envvar(
                request.data.get('envvars'))
        if request.data.get('envfiles'):
            request.data['envfiles'] = self._standardize_envfile(
                request.data.get('envfiles'))

        if request.data.get('command_type') == 'DEFAULT_COMMAND':
            request.data['command'] = ''

        try:
            job_config_serializer = JobConfigSerializer(data=request.data)
            job_config_serializer.is_valid(raise_exception=True)
            job_config_serializer.save()
        except:
            if request.data.get('schedule_rule'):
                MedusaClient.delete_schedule_event_config(
                    request.data['schedule_config_id'])
            raise
        return Response(data={'config_uuid': request.data['config_uuid']},
                        status=status.HTTP_201_CREATED)

    @tracing_response_time()
    def update(self, request, config_uuid):
        LOG.info('update a job config with : ' + json.dumps(request.data))
        self.validate_data(request.data)

        job_config = get_object_or_404(self.job_config_query_set,
                                       config_uuid=config_uuid)
        action = ''
        schedule_config_id = job_config.schedule_config_id
        schedule_config_secret_key = job_config.schedule_config_secret_key
        if schedule_config_id or request.data.get('schedule_rule'):
            if not schedule_config_id:
                action = 'create'
                rule = request.data.get('schedule_rule')
                build_callback_url = self._get_schedule_job_url(config_uuid)
                result = MedusaClient.create_schedule_event_config(rule, build_callback_url)
                request.data['schedule_config_id'] = result.get('id')
                request.data['schedule_config_secret_key'] = result.get('secret_key')
            elif not request.data.get('schedule_rule'):
                action = 'delete'
                request.data['schedule_config_id'] = ''
                request.data['schedule_config_secret_key'] = ''
                request.data['schedule_rule'] = ''
            else:
                action = 'update'
                request.data['schedule_config_id'] = schedule_config_id
                request.data['schedule_config_secret_key'] = schedule_config_secret_key

        if request.data.get('command_type') == 'DEFAULT_COMMAND':
            request.data['command'] = ''

        job_config_serializer = JobConfigForUpdateSerializer(job_config, data=request.data)
        try:
            job_config_serializer.is_valid(raise_exception=True)
        except:
            if action == 'create':
                MedusaClient.delete_schedule_event_config(request.data['schedule_config_id'])
            raise
        job_config_serializer.save()

        if action == 'delete':
            MedusaClient.delete_schedule_event_config(schedule_config_id)
        elif action == 'update':
            MedusaClient.update_schedule_event_config(
                request.data.get('schedule_rule'), schedule_config_id)
        return Response(status=status.HTTP_204_NO_CONTENT)

    @tracing_response_time()
    def delete(self, request, config_uuid):
        LOG.info('delete a job config with {}'.format(config_uuid))
        job_config = get_object_or_404(self.job_config_query_set,
                                       config_uuid=config_uuid)
        if job_config.schedule_config_id:
            MedusaClient.delete_schedule_event_config(
                job_config.schedule_config_id)

        with transaction.atomic():
            CronjobUtil.remove_jobs(config_uuid=config_uuid)
        job_config.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def _standardize_envvar(self, envvars):
        """fill in a empty dict for envvars, or transfer json string to dict"""
        if isinstance(envvars, str):
            envvars = json.loads(envvars)
        return envvars

    def _standardize_envfile(self, envfiles):
        """fill in a empty dict for envfiles, or transfer json string to dict"""
        if isinstance(envfiles, str):
            envfiles = json.loads(envfiles)
        return envfiles

    def validate_data(self, data):
        timeout = data.get('timeout')
        if timeout and str(timeout).isdigit():
            data['timeout'] = timeout
        else:
            data['timeout'] = settings.DEFAULT_TIMEOUT

    def _get_schedule_job_url(self, config_uuid):
        return '#ALAUDA_JAKIRO_ENDPOINT#/schedule_callback/jobs/?config_uuid={}'.format(config_uuid)