import uuid
from django.db import models
from jsonfield import JSONField


DEFAULT_COMMAND = 'DEFAULT_COMMAND'
CUSTOM_COMMAND = 'CUSTOM_COMMAND'
ENTRYPOINT_TYPE = ((DEFAULT_COMMAND, 'DEFAULT_COMMAND'),
                   (CUSTOM_COMMAND, 'CUSTOM_COMMAND')
                  )


class JobConfig(models.Model):

    config_uuid = models.CharField(max_length=36,
                                   primary_key=True,
                                   default=uuid.uuid4,
                                   blank=True)
    config_name = models.CharField(max_length=128)
    project_uuid = models.CharField(max_length=64, blank=True, default='')
    space_uuid = models.CharField(max_length=64, blank=True, default='')
    region_id = models.CharField(max_length=64)
    registry_index = models.CharField(max_length=64, blank=True, default='')
    registry_name = models.CharField(max_length=64, blank=True, default='')
    registry_project = models.CharField(max_length=64, blank=True, default='')
    image_name = models.CharField(max_length=64)
    image_tag = models.CharField(max_length=64, blank=True, default='latest')
    command_type = models.CharField(max_length=64,
                                    choices=ENTRYPOINT_TYPE,
                                    default=DEFAULT_COMMAND)
    command = models.CharField(max_length=128,
                               blank=True,
                               default='')
    last_job = JSONField(blank=True, default={})
    timeout = models.FloatField(default=1440.0,
                                blank=True)
    schedule_rule = models.CharField(max_length=64,
                                     default='',
                                     blank=True)
    schedule_config_id = models.CharField(max_length=36,
                                          default='',
                                          blank=True)
    schedule_config_secret_key = models.CharField(max_length=16,
                                                  default='',
                                                  blank=True)
    envvars = JSONField(blank=True, default=[])
    envfiles = JSONField(blank=True, default=[])
    cpu = models.FloatField(blank=True, default=0.5)
    memory = models.IntegerField(blank=True, default=512)
    created_by = models.CharField(max_length=64)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-updated_at',)
        db_table = 'tresdin_job_configs_job_config'

    def __str__(self):
        return self.config_uuid
