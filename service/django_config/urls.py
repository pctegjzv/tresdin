from django.conf import settings
from django.conf.urls import url, include
from django.http import HttpResponse
from diagnose.views import DiagnoseViewset


urlpatterns = [
    url(r'^ping/?$',
        lambda request: HttpResponse(
            'tresdin: If they want war, then we shall give it to them!')),
    url(r'^_ping/?$', lambda request: HttpResponse(
        'tresdin: If they want war, then we shall give it to them!')),
    url(r'^_auth_ping/?$', lambda request: HttpResponse(
        'tresdin: If they want war, then we shall give it to them!')),
    url(r'^_diagnose/?$', DiagnoseViewset.as_view({'get': 'retrieve'})),
    url(r'^{}/'.format(settings.API_VERSION), include([
        url('^instant-jobs', include('instant_jobs.urls'))
    ])),
    url(r'^{}/job_configs'.format(settings.API_VERSION), include(
        'job_configs.urls')),
    url(r'^{}/event_task'.format(settings.API_VERSION), include(
        'event_task.urls')),

    url(r'^{}/jobs'.format(settings.API_VERSION), include('jobs.urls')),
]
