import json
import logging
import traceback

from django.http.response import HttpResponse

from mekansm.exceptions import MekAPIException


logger = logging.getLogger(__name__)


class CommonExceptionMiddleware():

    def process_exception(self, request, exception):
        logger.error('Exception was not wrapped: {}'.format(str(exception)))
        logger.error(traceback.format_exc())
        exception = MekAPIException('unknown_issue', message=str(exception))
        error_response = {'errors': [exception.data]}
        return HttpResponse(json.dumps(error_response), content_type='application/json',
                            status=exception.status_code)

    def process_request(self, request):
        pass
