import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
SECRET_KEY = os.environ.get('SECRET_KEY', 'dfjfsadbf&*#adbSLSKS&*@(#basekfj!(*##HIBFEI*')
os.environ['SOURCE'] = '1046'
API_VERSION = 'v1'


DEBUG = os.getenv('DEBUG', 'False').lower() in ['true', 'yes', 'y']



ALLOWED_HOSTS = ['*']

INSTALLED_APPS = [
    'jobs',
    'job_configs',
    'event_task'
]

MIDDLEWARE_CLASSES = [
    'django_config.middleware.CommonExceptionMiddleware',
    'mekansm.contrib.django.middleware.MekAPIMiddleware'
]

ROOT_URLCONF = 'django_config.urls'

WSGI_APPLICATION = 'django_config.wsgi.application'

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (),
    'UNAUTHENTICATED_USER': None,
    'EXCEPTION_HANDLER': 'mekansm.contrib.rest_framework.exception_handler.exception_handler'
}


LOG_PATH = os.getenv('LOG_APTH', '/var/log/tresdin/')
os.makedirs(LOG_PATH, exist_ok=True)
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(asctime)s [%(levelname)s][%(threadName)s]' +
                      '[%(name)s:%(lineno)d] %(message)s'
        },
        'colored': {
            '()': 'colorlog.ColoredFormatter',
            'format': '%(log_color)s%(levelname)-8s%(reset)s %(blue)s%(message)s'
        }
    },
    'filters': {
        'require-debug-true': {
            '()': 'django.utils.log.RequireDebugTrue'
        },
        'require-debug-false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'console-dev': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'colored',
            'filters': ['require-debug-true']
        },
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
            'filters': ['require-debug-false']
        },
        'file-info': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'verbose',
            'filters': ['require-debug-false'],
            'filename': LOG_PATH + 'tresdin.info.log',
            'maxBytes': 1024 * 1024 * 1024,
            'backupCount': 1
        },
        'file-error': {
            'level': 'ERROR',
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'verbose',
            'filters': ['require-debug-false'],
            'filename': LOG_PATH + 'tresdin.error.log',
            'maxBytes': 1024 * 1024 * 1024,
            'backupCount': 1
        }
    },
    'loggers': {
        'django': {
            'handlers': ['console-dev', 'console', 'file-info', 'file-error'],
            'level': 'DEBUG' if DEBUG else 'INFO',
            'propagate': False
        },
        '': {
            'handlers': ['console-dev', 'console', 'file-info', 'file-error'],
            'level': 'DEBUG' if DEBUG else 'INFO'
        },
    }
}

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

def _get_db_engine(name):
    engines = {
        "postgresql": "django.db.backends.postgresql_psycopg2",
        "mysql": "django.db.backends.mysql"
    }
    if name in engines:
        return engines[name]
    else:
        return name

DATABASES = {'default': {
    'ENGINE': _get_db_engine(os.getenv('DB_ENGINE', 'django.db.backends.postgresql_psycopg2')),
    'NAME': os.getenv('DB_NAME'),
    'USER': os.getenv('DB_USER'),
    'HOST': os.getenv('DB_HOST'),
    'PASSWORD': os.getenv('DB_PASSWORD'),
    'PORT': os.getenv('DB_PORT', '5432'),
    'CONN_MAX_AGE': int(os.getenv('CONN_MAX_AGE', 3600)),
}}

# set pymysql escape attrabute
# refer to https://github.com/PyMySQL/PyMySQL/issues/273#issuecomment-64499080
def mysqldb_escape(value, conv_dict):
    from pymysql.converters import encoders
    vtype = type(value)
    # note: you could provide a default:
    # PY2: encoder = encoders.get(vtype, escape_str)
    # PY3: encoder = encoders.get(vtype, escape_unicode)
    encoder = encoders.get(vtype)
    return encoder(value)
if 'mysql' in _get_db_engine(os.getenv('DB_ENGINE', 'django.db.backends.postgresql_psycopg2')):
    import pymysql
    setattr(pymysql, 'escape', mysqldb_escape)

LOGGING_STORAGE = {
    'host': os.getenv('LOGGING_STORAGE_HOST') or 'http://es-int.alauda.cn',
    'port': os.getenv('LOGGING_STORAGE_PORT') or '9200',
    'username': os.getenv('LOGGING_STORAGE_USERNAME') or 'root',
    'password': os.getenv('LOGGING_STORAGE_PASSWORD') or 'rootpassword'
}

FURION_ENDPOINT = os.getenv('FURION_ENDPOINT', 'localhost:8080')

DEFAULT_JOB_SCHEDULER_TYPE = os.getenv('DEFAULT_JOB_SCHEDULER_TYPE', 'CHRONOS')
DEFAULT_JOB_SCHEDULER_ENDPOINT = os.getenv('DEFAULT_JOB_SCHEDULER_ENDPOINT', 'localhost:4400')
if DEFAULT_JOB_SCHEDULER_TYPE == 'KUBERNETES':
    DEFAULT_JOB_SCHEDULER_TOKEN = os.getenv('DEFAULT_JOB_SCHEDULER_TOKEN', '')
else:
    DEFAULT_JOB_SCHEDULER_USERNAME = os.getenv('DEFAULT_JOB_SCHEDULER_USERNAME', '')
    DEFAULT_JOB_SCHEDULER_PASSWORD = os.getenv('DEFAULT_JOB_SCHEDULER_PASSWORD', '')


JAKIRO_API_ENDPOINT = os.getenv('JAKIRO_API_ENDPOINT')
JAKIRO_API_VERSION = os.getenv('JAKIRO_API_VERSION', 'v1')
SVEN_ENDPOINT = os.getenv('SVEN_ENDPOINT')
SVEN_API_VERSION = os.getenv('SVEN_API_VERSION')
MEDUSA_ENDPOINT = os.getenv('MEDUSA_ENDPOINT')
MEDUSA_API_VERSION = os.getenv('MEDUSA_API_VERSION', 'v1')
WORKER_IMAGE = os.getenv('WORKER_IMAGE') or 'index.alauda.cn/alaudaorg/tresdin-worker:v1'
TECHIES_DEBUG = os.getenv('TECHIES_DEBUG', False)
DEFAULT_TIMEOUT = os.getenv('DEFAULT_TIMEOUT', 1440)

REQUEST_TIMEOUT = int(os.getenv('REQUEST_TIMEOUT', 10))
GRACE_PERIOD_SECONDS = os.getenv('GRACE_PERIOD_SECONDS', 60)

LIST_PAGE = 1
LIST_PAGE_SIZE = 20
UUID_PATTERN = '[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}'

SYNC_COUNT = os.getenv('SYNC_COUNT', '3')
STATUS_SYNC_RULE = os.getenv('STATUS_SYNC_RULE', '* * * * *')

CONTAINER_MANAGER='CONTAINER_MANAGER'

# timeout for request k8s event, second
EVENT_TIMEOUT = int(os.getenv('EVENT_TIMEOUT', 10))
# event task sleep max intercal, second
EVENT_MAX_INTERVAL = int(os.getenv('EVENT_MAX_INTERVAL', 4))
# enable event task handler, default is true
EVENT_TASK_ENABLE=os.getenv('EVENT_TASK_ENABLE', 'True').lower() in ['true', 'yes', 'y']
# make supervisord consider event_task run success(then not restart it)
# if event_task exit more than startsecs(supervisord config, default 1 second)
EVENT_TASK_STARTSECS=int(os.getenv('EVENT_TASK_STARTSECS', 1))

TRESDIN_ENDPOINT = os.getenv('TRESDIN_ENDPOINT', 'http://localhost')

CUR_TIME_ZONE = os.getenv('CUR_TIME_ZONE', 'UTC')
TIME_ZONE = 'UTC'

JOB_CREATE_RULE = '* */3 * * *'
JOB_DELETE_RULE = '*/30 * * * *'