from .base import *  # NOQA

DEBUG = True

DATABASES = {'default': {
    'ENGINE': 'django.db.backends.sqlite3',
    'NAME': 'tresdin_dev.db',
    'USER': '',
    'HOST': "127.0.0.1",
    'PORT': "6432",
}}