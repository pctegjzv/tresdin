from django.conf.urls import url

from .views import InstantJobViewSet

JOB_NAME_PATTERN = '[\w_-]*'


urlpatterns = [
    url(r'^/?$',
        InstantJobViewSet.as_view({
            'post': 'create',
        })
    ),
    url(r'^/(?P<job_name>{})/?$'.format(JOB_NAME_PATTERN),
        InstantJobViewSet.as_view({
            'get': 'retrieve',
            'delete': 'delete',
            'post': 'job_callback'
        })
    )
]
