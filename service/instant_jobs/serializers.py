from rest_framework import serializers


NETWORK_HOST_TYPE = 'HOST'
NETWORK_BRIDGE_TYPE = 'BRIDGE'
NETWORK_TYPES = [NETWORK_HOST_TYPE, NETWORK_BRIDGE_TYPE]


class EnvironmentVarField(serializers.Serializer):
    name = serializers.CharField()
    value = serializers.CharField(allow_blank=True)


class VolumeField(serializers.Serializer):
    host_path = serializers.CharField()
    container_path = serializers.CharField()
    mode = serializers.CharField()


class InstantJobSerializer(serializers.Serializer):
    region_id = serializers.UUIDField(required=False)
    name = serializers.CharField()
    image = serializers.CharField()
    command = serializers.CharField(required=False, allow_blank=True)
    privileged = serializers.BooleanField(default=False)
    cpu = serializers.FloatField()
    memory = serializers.IntegerField()
    network = serializers.ChoiceField(choices=NETWORK_TYPES)
    environment_variables = EnvironmentVarField(many=True, required=False)
    volumes = VolumeField(many=True, required=False)
    namespace = serializers.CharField(required=False, allow_blank=True)
    env = serializers.JSONField(required=False, allow_null=True, default=[])
    envFrom = serializers.JSONField(required=False, allow_null=True, default=[])
