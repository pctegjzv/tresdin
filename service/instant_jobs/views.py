import logging

from django.conf import settings
from rest_framework import fields, viewsets, status
from rest_framework.response import Response

from alauda_job.job_client import AlaudaJob
from alauda.tracing import tracing_response_time

from region import Region, SchedulerConfig

from .serializers import InstantJobSerializer
from commons.medusa_client import MedusaClient

from kubernetes.client.rest import ApiException


logger = logging.getLogger(__name__)
SUCCEEDED = 'succeeded'
WAITING = 'waiting'
RUNNING = 'running'
FAILED = 'failed'


class InstantJobViewSet(viewsets.ViewSet):
    prefix_allowed = ('private_build_', 'alauda_work_', 'cd_task_')

    def _get_job_client(self, region_id=None):
        if not region_id:
            scheduler_type = settings.DEFAULT_JOB_SCHEDULER_TYPE
            endpoint = settings.DEFAULT_JOB_SCHEDULER_ENDPOINT
            if scheduler_type == 'KUBERNETES':
                auth = {
                    'token': settings.DEFAULT_JOB_SCHEDULER_TOKEN
                }
            else:
                auth = {
                    'username': settings.DEFAULT_JOB_SCHEDULER_USERNAME,
                    'password': settings.DEFAULT_JOB_SCHEDULER_PASSWORD
                }
            c = {
                'scheduler_type': scheduler_type,
                'endpoint': endpoint,
                'auth': auth
            }
            scheduler_config = SchedulerConfig(**c)
        else:
            region = Region(region_id)
            scheduler_config = region.get_job_scheduler_config()
        logger.debug("scheduler_type: {}; is_new_k8s: {}".format(
            scheduler_config.scheduler_type, scheduler_config.is_new_k8s))
        return scheduler_config, AlaudaJob(scheduler_config)

    def _adjust_job_name(self, scheduler, job_name):
        if scheduler.scheduler_type == 'KUBERNETES':
            for keyword in self.prefix_allowed:
                if job_name.startswith(keyword):
                    return keyword.replace('_', '-') + job_name[len(keyword):]
        return job_name

    @tracing_response_time()
    def create(self, request):
        logger.debug('Creating instant job ...')
        serializer = InstantJobSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        scheduler_config, job_client = self._get_job_client(
            serializer.validated_data.get('region_id'))
        job_name = self._adjust_job_name(scheduler_config, serializer.validated_data['name'])
        serializer.validated_data['name'] = job_name
        job_client.create_job(serializer.validated_data)
        # create medusa callback job
        self._create_job_callback(settings.JOB_CREATE_RULE, job_name,
                                  serializer.validated_data.get('region_id'),
                                  serializer.validated_data.get('namespace', 'default'))
        return Response(None, status=status.HTTP_204_NO_CONTENT)

    @tracing_response_time()
    def retrieve(self, request, job_name):
        region_id = request.query_params.get('region_id')
        namespace = request.query_params.get('namespace')
        logger.debug('Retrieving instant job {} from {} ...'.format(job_name, namespace))
        scheduler_config, job_client = self._get_job_client(region_id)
        job_name = self._adjust_job_name(scheduler_config, job_name)
        data = job_client.get_job(job_name, namespace=namespace)
        return Response(data)

    @tracing_response_time()
    def delete(self, request, job_name):
        region_id = request.query_params.get('region_id')
        namespace = request.query_params.get('namespace')
        logger.debug('Deleting instant job {} from {} ...'.format(job_name, namespace))
        scheduler_config, job_client = self._get_job_client(region_id)
        job_name = self._adjust_job_name(scheduler_config, job_name)
        job_client.delete_job(job_name, namespace=namespace)
        return Response(None, status=status.HTTP_204_NO_CONTENT)

    def job_callback(self, request, job_name):
        region_id = request.query_params.get('region_id')
        namespace = request.query_params.get('namespace')
        logger.info('medusa callback to delete job: {}, namespace: {}'
                    .format(job_name, namespace))
        scheduler_config, job_client = self._get_job_client(region_id)
        job_name = self._adjust_job_name(scheduler_config, job_name)
        try:
            job_data = job_client.get_job(job_name, namespace=namespace)
        except ApiException as e:
            if e.status == 404:
                logger.info('job {} does not exist '.format(job_name))
                return Response(status=status.HTTP_410_GONE)
            else:
                return Response('k8s get job failed: {}'.format(job_name))
        except Exception as e:
            logger.error('tredsin get job {} failed, {}'.format(job_name, e))
            return Response('get job failed: {}'.format(job_name))

        '''
            if job is running or waiting, return 200
            if job is succeeded or failed, delete job:
               delete failed, create another callback
               delete success, return 410 to delete callback
            if job does not exist , return 410
        '''
        if job_data.get('status') in (RUNNING, WAITING):
            logger.info('medusa callback: job {} is running or waiting'.format(job_name))
            return Response('job is running or waiting')
        elif job_data.get('status') in(SUCCEEDED, FAILED):
            try:
                logger.info('medusa callback: job is {}, delete job'.format(job_name))
                job_client.delete_job(job_name, namespace=namespace)
                return Response(status=status.HTTP_410_GONE)
            except Exception as e:
                logger.error('medusa callback: delete job: {} failed, {}'.format(job_name, e))
                self._create_job_callback(settings.JOB_DELETE_RULE, job_name, region_id, namespace)
                return Response('delete job failed: {}'.format(job_name))
        else:
            logger.error('job {} status is not excepted, job_data is {} '.format(job_name, job_data))
            return Response('get job failed: {}, job data is {}'.format(job_name, job_data), status=status.HTTP_200_OK)

    def _create_job_callback(self, rule, job_name, region_id, namespace):
        try:
            webhook_url = self._get_schedule_job_url(job_name, region_id, namespace)
            logger.info('rule is: {}, webhook_url is: {}'.format(rule, webhook_url))
            MedusaClient.create_schedule_event_config(rule, webhook_url)
        except Exception as e:
            logger.error('create_job_callback error: {}, region_id is {}, namespace is {}, job_name is {},'.format(e, region_id, namespace, job_name))
            pass

    def _get_schedule_job_url(self, job_name, region_id, namespace):
        return '#ALAUDA_TRESDIN_ENDPOINT#/v1/instant-jobs/{}/?region_id={}&namespace={}'\
            .format(job_name, region_id, namespace)
