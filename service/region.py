from alauda.furion.client import FurionClient

from django.conf import settings


class SchedulerConfig:
    def __init__(self, scheduler_type='', endpoint='', auth=None, is_new_k8s=False):
        self.scheduler_type = scheduler_type
        self.endpoint = endpoint
        self.auth = auth or dict()
        self.is_new_k8s = is_new_k8s


class Region():

    def __init__(self, region_id):
        self._client = FurionClient(endpoint=settings.FURION_ENDPOINT)
        self.region_id = region_id

    def get_job_scheduler_config(self):
        region_info = self._client.get_region(self.region_id)
        if region_info.get('container_manager') == 'KUBERNETES':

            manager = region_info['features']['service']['manager']
            platform_version = region_info['platform_version']
            c = {
                'scheduler_type': 'KUBERNETES',
                'endpoint': manager['endpoint'],
                'auth': {
                    'token': manager['token']
                },
                "is_new_k8s": platform_version == "v3"
            }
            return SchedulerConfig(**c)
        else:
            if 'tunnel' not in region_info['features']:
                endpoint = 'http://{}:4400'.format(
                    region_info['features']['service']['exec']['endpoint'])
            else:
                endpoint = 'http://{}'.format(
                    region_info['features']['tunnel']['port_mapping']['doom'])
            c = {
                'scheduler_type': 'CHRONOS',
                'endpoint': endpoint,
                'auth': {
                    'username': None,
                    'password': None
                }
            }
            return SchedulerConfig(**c)
