import logging

from rest_framework import fields, viewsets, status
from rest_framework.response import Response
from django.shortcuts import get_object_or_404

from alauda.tracing import tracing_response_time

from event_task.models import EventTask, COMPLETE_STATUS
from event_task.serializers import EventTaskSerializer
from jobs.client import FurionClient

from region import Region

logger = logging.getLogger(__name__)


class EventTaskViewSet(viewsets.ViewSet):
    task_query_set = EventTask.objects.all()

    @tracing_response_time()
    def create(self, request):
        task_serializer = EventTaskSerializer(data=request.data)
        task_serializer.is_valid(raise_exception=True)
        # check if register cluster manager is kubernetes
        region = Region(request.data['region_id'])
        scheduler_config = region.get_job_scheduler_config()
        if scheduler['scheduler_type'] != 'KUBERNETES':
            raise Exception('region {} not support k8s event'.format(region_id))
        try:
            task_serializer.save()
        except Exception as e:
            logger.debug('create event task to db error {}'.format(e.message))
            raise

        return Response(status=status.HTTP_200_OK)

    @tracing_response_time()
    def get(self, request):
        """
        Return task in waiting status
        """
        try:
            task = EventTask.objects.all().exclude(status__in=COMPLETE_STATUS).latest()
        except:
            logger.debug('get no task, continue...')
            return Response(status=status.HTTP_204_NO_CONTENT)
        
        task_serializers = EventTaskSerializer(task)
        return Response(data=task_serializers.data)

    @tracing_response_time()
    def delete(self, request, task_uuid):
        """
        Delete event_task with task_uuid

        :param task_uuid: id of event task
        :type task_uuid: str
        """
        logger.info('delete event task with id {}'.format(task_uuid))
        event_task = get_object_or_404(self.task_query_set,
                                       id=task_uuid)
        event_task.delete()
        return Response(status=status.HTTP_200_OK)