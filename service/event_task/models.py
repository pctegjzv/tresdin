from django.db import models
import uuid 

STATUS_IN_PROGRESS = 'I'
STATUS_FAILED = 'F'
STATUS_WAITING = 'W'
STATUS_WEIGHTS = {
    STATUS_WAITING: 20,
    STATUS_IN_PROGRESS: 30,
    STATUS_FAILED: 40,
}

STATUS = (
    (STATUS_IN_PROGRESS, STATUS_IN_PROGRESS),
    (STATUS_FAILED, STATUS_FAILED),
    (STATUS_WAITING, STATUS_WAITING)
)

COMPLETE_STATUS = [STATUS_FAILED, STATUS_IN_PROGRESS]


class EventTask(models.Model):
    id = models.CharField(max_length=36, primary_key=True, default=uuid.uuid4, editable=False)
    region_id = models.CharField(max_length=64, null=True, blank=True)
    namespace = models.CharField(max_length=64, default='default')
    job_name = models.CharField(max_length=64) # private-build- + len(uuid4) = 14+36 = 50
    token = models.CharField(max_length=64)
    host = models.CharField(max_length=32)
    status = models.CharField(max_length=8, choices=STATUS, default=STATUS_WAITING)
    created_at = models.DateTimeField(auto_now_add=True, db_index=True)
    started_at = models.DateTimeField(null=True, blank=True)
    ended_at = models.DateTimeField(null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('created_at',)
        get_latest_by = 'created_at'
    
    def __str__(self):
        return 'event_task:{}'.format(self.job_name)
