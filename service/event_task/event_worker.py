import time
import datetime
import threading
import logging
import os

from kubernetes import watch
from kubernetes.client import Configuration, CoreV1Api, ApiClient
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db.utils import OperationalError, DatabaseError
from django.db import connection, transaction

from mekansm.request import MekRequest

from event_task.models import EventTask, COMPLETE_STATUS, STATUS_IN_PROGRESS
from event_task.serializers import EventTaskSerializer
from jobs.log_client.log_client import get_es_client

logger = logging.getLogger(__name__)


class TresdinRequest(MekRequest):
    endpoint = '{}/{}'.format(settings.TRESDIN_ENDPOINT,
                              settings.API_VERSION)
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'User-Agent': 'tresdin/v1.0'
    }

class TresdinClient(object):
    timeout = settings.REQUEST_TIMEOUT
    @classmethod
    def get_task(cls):
        return TresdinRequest.send('{}/'.format('event_task'),
                                   method='GET', timeout=TresdinClient.timeout)['data']


class EventHandler(threading.Thread):

    def __init__(self, task):
        """Init
        :param task: event task
        :type task: dict of event_task.models.EventTask
        """
        super().__init__()
        if isinstance(task, EventTask):
            self.task_id = task.id
            self.job_name = task.job_name
            self.region_id = task.region_id
            self.token = task.token
            self.host = task.host
            self.namespace = task.namespace
        elif isinstance(task, dict):
            self.task_id = task['id']
            self.job_name = task['job_name']
            self.region_id = task['region_id']
            self.token = task['token']
            self.host = task['host']
            self.namespace = task['namespace']
        else:
            raise Exception('event task type {} invalid'.format(type(task)))

    def run(self):
        self.process_build_events()

    def process_build_events(self):
        """
        Retrive k8s job events and save to log store(elasticsearch)
        This method is common for k8s
        """
        count = 0
        log_client = get_es_client(self.region_id)
        w = watch.Watch()

        config = Configuration()
        config.verify_ssl = False
        config.api_key_prefix = {'authorization': 'Bearer'}
        config.api_key = {'authorization': self.token}
        config.host = self.host
        pod_client = CoreV1Api(api_client=ApiClient(configuration=config))

        logger.info('start handle events for job {}'.format(self.job_name))
        for event in w.stream(pod_client.list_namespaced_event,
                              timeout_seconds=settings.EVENT_TIMEOUT,
                              namespace=self.namespace):
            raw_event = event['raw_object']
            if self.job_name in raw_event['involvedObject']['name']:
                msg = self._gen_event_msg(raw_event)
                log_client.save_job_log(self.job_name, msg)
                count += 1
        
        logger.info('get {} event for job {} within {}s'.format(count,
                                                                self.job_name,
                                                                settings.EVENT_TIMEOUT))
        self.delete_task()
        logger.info('handle events for job {} done'.format(self.job_name))
    
    def delete_task(self):
        """
        Delete task with id
        """
        task = EventTask.objects.filter(id=self.task_id)
        task.delete()

    def _get_pod_name(self, message):
        name = message[len('Created pod:'):]
        return str.strip(name)

    def _gen_event_msg(self, event):
        """
        Generate event message

        :param event: event from k8s event(raw object)
        :type event: dict
        :returns: message
        :rtype: str
        """

        msg = '[{}][{}][{}] > {}'.format(str.title(event['source']['component']),
                                         event['involvedObject']['kind'],
                                         event['reason'],
                                         event['message'])
        return msg

def reconnect_database_on_error(func):
    """
    Decorator to reconnect to the database if the connection is closed in Django
    refer to jakiro code and https://stackoverflow.com/questions/4447497/django-how-to-reconnect-after-databaseerror-query-timeout
    """

    def process_connection_exception(*args, **kwargs):
        result = None
        try:
            result = func(*args, **kwargs)
        except (DatabaseError, OperationalError) as err:
            logger.info('[EventTask] Database connection error: {}'.format(err))
            connection.connection.close()
            connection.connection = None
        except ObjectDoesNotExist as ne:
            logger.info('[EventTask] {}'.format(ne))
        except Exception as exc:
            logger.error('[EventTask] Some other exception: {}'.format(exc))
            # return exc
        return result
    return process_connection_exception

class EventWorker():
    @classmethod
    @reconnect_database_on_error
    def get_task(cls):
        """Get first waiting task with FIFO order

        :returns: task
        :rtype: dict or None
        """
        try:
            event_task = EventTask.objects.all().exclude(status__in=COMPLETE_STATUS).latest()
        except Exception as e:
            logger.debug('get task from db run into exception {}, will return none'.format(e))
            return None
        return event_task

    @classmethod
    @reconnect_database_on_error
    def handle_event(cls):
        """
        Get first waiting task with FIFO order and promise consistency
        """
        found = False
        with transaction.atomic():
            event_task = EventTask.objects.select_for_update().exclude(status__in=COMPLETE_STATUS).latest()
            if not event_task:
                # logger.info('get no k8s event task, will try again')
                return found

            logger.info('get task {}, will start it.'.format(event_task))
            event_task.status = STATUS_IN_PROGRESS
            event_task.started_at = datetime.datetime.now()
            event_task.save()
            worker = EventHandler(event_task)
            worker.start()
            found = True

        return found

    @classmethod
    def start(cls):
        default_sleep=1
        max_sleep=settings.EVENT_MAX_INTERVAL
        while True:
            try:
                found = cls.handle_event()
            except Exception as e:
                logger.error('handle event task exception {}'.format(e))
            if found:
                default_sleep=1
                continue

            logger.info('get no k8s event task, will try next after {} seconds'.format(default_sleep))
            time.sleep(default_sleep)
            default_sleep = default_sleep + 1 if default_sleep < max_sleep else max_sleep