from django.conf.urls import url
from event_task.views import EventTaskViewSet
from django.conf import settings


urlpatterns = [
    url(r'^/?$',
        EventTaskViewSet.as_view(
            {
                'post': 'create',
                'get': 'get'
            }
        )),
    url(r'^/(?P<task_uuid>{})/?$'.format(settings.UUID_PATTERN),
        EventTaskViewSet.as_view(
            {
                'delete': 'delete'
            }
        ))
]