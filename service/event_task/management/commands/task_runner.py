import logging
import sys
import time

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from event_task.event_worker import EventWorker

logger = logging.getLogger(__name__)

class Command(BaseCommand):
    """
    Run task_runner, runner fetch event from db one seconds a time.
    If get one, will run it in a new thread.
    
    Example usage:
    python manage.py rqworker
    """

    def handle(self, *args, **options):
        try:
            if settings.EVENT_TASK_ENABLE:
                logger.info('EventTask is enable, will start it...')
                EventWorker.start()
            else:
                logger.info('EventTask is disable, do nothing...')
                time.sleep(settings.EVENT_TASK_ENABLE + 1)
                sys.exit(0)
        except CommandError as e:
            logger.error('execute command error: {}'.format(e.message))