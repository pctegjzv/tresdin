from rest_framework import serializers

from event_task.models import EventTask

class EventTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventTask
        fields = '__all__'